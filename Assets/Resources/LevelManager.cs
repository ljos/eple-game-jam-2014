﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour{

	void Start(){}
	void Update(){}

	void Awake(){
		DontDestroyOnLoad(gameObject);
		DontDestroyOnLoad(this);
	}
	
	public void changeLevel(levels level)
	{
		switch(level)
		{
		case levels.Overworld:
			Application.LoadLevel(levels.Overworld.ToString());
			break;
		case levels.TestBane1:
			Application.LoadLevel(levels.TestBane1.ToString());
			break;
		case levels.Bane2:
			Application.LoadLevel(levels.Bane2.ToString());
			break;
		case levels.Bane3:
			Application.LoadLevel(levels.Bane3.ToString());
			break;
		case levels.Bane4:
			Application.LoadLevel(levels.Bane4.ToString());
			break;
		case levels.Stillscene:
			Application.LoadLevel(levels.Stillscene.ToString());
			break;
		default:
			break;
		}
	}
	public enum levels{Overworld, TestBane1, Bane2, Bane3, Bane4, Stillscene}
}
