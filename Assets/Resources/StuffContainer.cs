﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class StuffContainer : MonoBehaviour {

	private static bool Evil, Grownup, Choice;
	private static bool door1, door2, door3, door4, door5;
	private static int ev, grown;

	void OnStart()
	{
	}
	void Awake(){
		DontDestroyOnLoad(gameObject);
		DontDestroyOnLoad(this);
	}

	void Update()
	{
		door1 = true;

		if(Choice)
		{
			if(Evil)
			{
				ev += 1;
			}
			if(Grownup)
			{
				grown += 1;
			}
			Choice = false;
		}
	}
	public void setBoolTrue(int doorNumb)
	{
		switch(doorNumb)
		{
		case 1:
			door1 = true;
			break;
		case 2:
			door2 = true;
			break;
		case 3:
			door3 = true;
			break;
		case 4:
			door4 = true;
			break;
		case 5:
			door5 = true;
			break;
		}
	}
	public bool getDoor(int doorNumby)
	{
		switch(doorNumby)
		{
		case 1:
			return door1;
			break;
		case 2:
			return door2;
			break;
		case 3:
			return door3;
			break;
		case 4:
			return door4;
			break;
		case 5:
			return door5;
			break;
		}
		return false;
	}
	public bool getsetevil
	{
		get{return Evil;}
		set{Evil = value;}
	}
	public bool setchoice
	{
		set{Choice = value;}
	}
	public bool getsetgrownup
	{
		get{return Grownup;}
		set{Grownup = value;}
	}
	public int getNUMBEvil
	{
		get{return ev;}
	}
	public int getNUMBgrown
	{
		get{return grown;}
	}
}
