﻿using UnityEngine;
using System.Collections;

public class Swingdoor : MonoBehaviour {

	int moveDirection; //-1, 0, 1 for in,stop,out respectively
	float rotation;

	Rigidbody rigid;


	public float rotationSpeed = 1;
	public float maxopen = 160;
	
	// Use this for initialization
	void Start () {	
		moveDirection = 0;
		rotation = 0;
		rotation = 0;
	}
	
	public void toggleOpenDoor(){
		if (moveDirection == 0) {
			if(rotation >= maxopen)
				moveDirection = -1;
			else if (rotation <= 0)
				moveDirection = 1;
		} else {
			moveDirection = -moveDirection;
		}
	}

	void movedoor(){
		if(moveDirection != 0){
			float rotationMovement = rotationSpeed * moveDirection;

			transform.Rotate(0, 0, rotationMovement);
			rotation += rotationMovement;

			
			if(rotation >= maxopen || rotation <= 0)
				moveDirection = 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
		movedoor ();
	}
}
