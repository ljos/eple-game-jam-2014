﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]
public class OverworldCharacter : MonoBehaviour
{

		CharacterController charctrlr;
		Vector3 pos = new Vector3 (0f, 0f, 0f);
		[Range(0,20)]
		public float
				rotateMultiplier;
		[Range(0,5)]
		public float
				speedMultiplier;

		// Use this for initialization
		void Start ()
		{
				charctrlr = GetComponent<CharacterController> ();
				rotateMultiplier = 5f;
				speedMultiplier = 1f;
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
				float hor = Input.GetAxisRaw ("Horizontal");

				var rotateSpeed = hor * 5f;
		

				// Rotate around y - axis
				transform.Rotate (0, rotateSpeed, 0);
		
				var direction = transform.TransformDirection (Vector3.forward);

				if (Input.GetKeyDown (KeyCode.UpArrow)) {
						direction = transform.TransformDirection (Vector3.forward);
				}
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
						direction = transform.TransformDirection (Vector3.back);
				}

				// Move forward / backward
				var curSpeed = speedMultiplier * Input.GetAxis ("Vertical");
				charctrlr.SimpleMove (direction * curSpeed);
		}
}
