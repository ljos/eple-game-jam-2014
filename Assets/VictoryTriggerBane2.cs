﻿using UnityEngine;
using System.Collections;

public class VictoryTriggerBane2 : MonoBehaviour {

	bool backToOverWorld;
	string text = "Press 'E' to play return to the House";
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown (KeyCode.E) && backToOverWorld) {
			GameObject keeper = GameObject.Find("TheKeeper");
			if(backToOverWorld)
			{
				keeper.GetComponent<StuffContainer>().setBoolTrue(2);
			}
			keeper.GetComponent<LevelManager>().changeLevel (LevelManager.levels.Overworld);
		}
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log ("Did Trigger");
		backToOverWorld = true;
	}
	
	void OnGUI()
	{
		if (backToOverWorld) 
		{
			var width = 250;
			var height = 50;
			var centeredStyle = GUI.skin.GetStyle("Label");
			centeredStyle.fontSize = 20;
			centeredStyle.alignment = TextAnchor.LowerCenter;
			var test = centeredStyle.CalcSize (new GUIContent(text));
			var rect = new Rect ((Screen.width)/2 - (test.x/2),(Screen.height) - 60 ,test.x,test.y);
			GUI.Box (rect,"");
			GUI.Label (rect, text,centeredStyle);
		}
	}
}
