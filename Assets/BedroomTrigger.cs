﻿using UnityEngine;
using System.Collections;

public class BedroomTrigger : MonoBehaviour {
	
	bool textShow = false;
	string text = "Press 'E' to play in mom and dads bedroom.";
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.E) && textShow) {
			GameObject keeper = GameObject.Find("TheKeeper");
			keeper.GetComponent<LevelManager>().changeLevel (LevelManager.levels.TestBane1);
		}
		
	}
	void OnTriggerEnter(Collider other)
	{
		textShow = true;
		
	}
	void OnTriggerExit(Collider other){
		textShow = false;
	}
	void OnGUI()
	{
		if (textShow) 
		{
			var width = 250;
			var height = 50;
			var centeredStyle = GUI.skin.GetStyle("Label");
			centeredStyle.fontSize = 20;
			centeredStyle.alignment = TextAnchor.LowerCenter;
			var test = centeredStyle.CalcSize (new GUIContent(text));
			var rect = new Rect ((Screen.width)/2 - (test.x/2),(Screen.height) - 60 ,test.x,test.y);
			GUI.Box (rect,"");
			GUI.Label (rect, text,centeredStyle);
		}
	}
}
