﻿using UnityEngine;
using System.Collections;

public class UnlockAreas : MonoBehaviour {

	Swingdoor[] allSwingdoors;
	UnFogOfWar[] allUfow;
	bool[] isUnlocked;
	GameObject keeper;

	public int nextArea;

	// Use this for initialization
	void Start () {	
		keeper = GameObject.Find("TheKeeper");
		nextArea = 0;
		allSwingdoors = gameObject.GetComponentsInChildren<Swingdoor>();
		allUfow = gameObject.GetComponentsInChildren<UnFogOfWar> ();
		isUnlocked = new bool[allSwingdoors.Length];
	}

	[ContextMenu ("UnlockArea0")]
	void UnlockArea0(){
		UnlockArea (0);
	}
	[ContextMenu ("UnlockArea1")]
	void UnlockArea1(){
		UnlockArea (1);
	}
	[ContextMenu ("UnlockArea2")]
	void UnlockArea2(){
		UnlockArea (2);
	}
	[ContextMenu ("UnlockArea3")]
	void UnlockArea3(){
		UnlockArea (3);
	}


	void UnlockArea(int i){

		if (i < allSwingdoors.Length) {
			if(!isUnlocked[i]){
				allSwingdoors[i].toggleOpenDoor ();
				allUfow[i].unFogOfWar();
				isUnlocked[i] = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(keeper.GetComponent<StuffContainer>().getDoor(1))
		{
			UnlockArea(0);
		}
		if(keeper.GetComponent<StuffContainer>().getDoor(2))
		{
			UnlockArea(1);
		}
		if(keeper.GetComponent<StuffContainer>().getDoor(3))
		{
			UnlockArea(2);
		}
		if(keeper.GetComponent<StuffContainer>().getDoor(4))
		{
			UnlockArea(3);
		}

	}
}
