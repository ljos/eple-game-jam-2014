﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlatformMove))]
public class TwoDAnimation : MonoBehaviour {
	private Animator _anim;
	private PlatformMove _platmove;
	// Use this for initialization
	void Start () {
		_anim = GetComponent<Animator>();
		_platmove = GetComponent<PlatformMove> ();
	}
	
	// Update is called once per frame
	void Update () {


		if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			_anim.SetBool("Running",true);
		}
		else if(Input.GetKeyUp(KeyCode.D)|| Input.GetKeyUp(KeyCode.RightArrow))
		{
			_anim.SetBool("Running",false);
		} else if (!_platmove.grounded) {
			_anim.SetBool ("IsClimbing", true);
		} else if(_platmove.grounded) {
			_anim.SetBool ("IsClimbing", false);
		}
		else if(Input.GetKeyDown(KeyCode.E))
		{
			_anim.SetBool("IsEvil",true);
		}
		else if(Input.GetKeyDown(KeyCode.N))
		{
			_anim.SetBool("IsEvil",false);
		}


	}
}
