﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class ForwardAndBackward : MonoBehaviour {

	public float speed=0.1f;
	public int frameTurn=50;

	bool forward;
	int currentFrame;

	Animator anim;


	// Use this for initialization
	void Start () {

		currentFrame = 0;
		forward = true;
		anim = GetComponent<Animator> ();
		anim.SetBool ("Running", true);
	}

	public void setEvil(bool isevil){
		anim.SetBool ("IsEvil", isevil);
		Debug.Log(isevil);
	}
	
	// Update is called once per frame
	void Update () {

		if (forward) {
			if(currentFrame < frameTurn){
				currentFrame++;
				var currPos = gameObject.transform.position;
				currPos.x += speed;
				gameObject.transform.position = currPos;
			} else {
				forward = !forward;
			}
		} else {
			if(currentFrame > 0){
				currentFrame--;
				var currPos = gameObject.transform.position;
				currPos.x -= speed;
				gameObject.transform.position = currPos;
			} else {
				forward = !forward;
			}
		}
		
	}
}
