﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class UnFogOfWar : MonoBehaviour {

	MeshRenderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent<MeshRenderer>();
	}

	public void unFogOfWar(){
		rend.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}