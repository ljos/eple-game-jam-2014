﻿using UnityEngine;
using System.Collections;

public class StaticObject : MonoBehaviour {

	Vector3 pos;
	Rigidbody2D rig;
	// Use this for initialization
	void Start () {
		rig = GetComponent<Rigidbody2D>();
		pos = rig.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		rig.transform.position = pos;
	}
}
