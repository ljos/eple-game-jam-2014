﻿using UnityEngine;
using System.Collections;

public class cameraFollow : MonoBehaviour {

	public Transform target;
	public float jiggleroom;

	// Use this for initialization
	void Start () {
	
	}
	

	void Update(){
		var myPos = gameObject.transform.position;
		var tarPos = target.transform.position;
		if (Mathf.Abs (myPos.x - tarPos.x) >= jiggleroom) {
			myPos.x = tarPos.x + ((myPos.x < tarPos.x) ? -jiggleroom:(jiggleroom));
		}

		gameObject.transform.position = myPos;
	}
}
