﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class CactusCollision : MonoBehaviour {

	BoxCollider2D deadlyEnd;

	// Use this for initialization
	void Start () {
		deadlyEnd = GetComponent<BoxCollider2D> ();
	}

	void OnTriggerEnter2D(Collider2D coll){
		Player player = coll.GetComponent<Player> ();
		player.die ();
	}

	// Update is called once per frame
	void Update () {
	
	}
}
