﻿using UnityEngine;
using System.Collections;


public class Main : MonoBehaviour {

	GuiHandler guiHandle;
	// Use this for initialization
	void Start () {
		guiHandle = new GuiHandler();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		guiHandle.DrawGUI();
	}
}
