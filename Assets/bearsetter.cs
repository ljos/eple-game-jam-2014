﻿using UnityEngine;
using System.Collections;

public class bearsetter : MonoBehaviour {

	GameObject keeper;
	ForwardAndBackward[] children;
	// Use this for initialization
	void Start () {
		keeper = GameObject.Find("TheKeeper");
		children = GetComponents<ForwardAndBackward>();
		foreach(ForwardAndBackward fb in children)
		{
			fb.setEvil(keeper.GetComponent<StuffContainer>().getsetevil);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
