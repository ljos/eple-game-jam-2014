﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlatformMove))]
[RequireComponent(typeof(SpriteRenderer))]
public class Player : MonoBehaviour {

	PlatformMove mover;
	SpriteRenderer renderer;

	// Use this for initialization
	void Start () {
		mover = GetComponent<PlatformMove> ();
		renderer = GetComponent<SpriteRenderer> ();
	
	}

	public void die(){
		mover.setDead ();
	}
}
