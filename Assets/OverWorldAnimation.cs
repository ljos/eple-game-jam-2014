﻿using UnityEngine;
using System.Collections;

public class OverWorldAnimation : MonoBehaviour {
	private Animator _anim;
	// Use this for initialization
	void Start () {
		_anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

		var hor = Input.GetAxisRaw ("Vertical");

		if (hor == 1) {
			_anim.SetBool("Running", true);
		} else {
			_anim.SetBool("Running", false);
		}

	}
}
