﻿using UnityEngine;
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Security.Cryptography;

public class XmlManager
{
	public string _Filepath;
	
	// Use this for initialization
	void Start () {
		_Filepath = Application.dataPath;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	byte[] StringToUTF8ByteArray(string pXmlString)
	{
		UTF8Encoding encoding = new UTF8Encoding();
		byte[] byteArray = encoding.GetBytes(pXmlString);
		return byteArray;
	}
	
	// Here we serialize our UserData object of myData 
	public XmlDocument SerializeObject(object pObject)
	{
		XmlSerializer xmlSer = new XmlSerializer(pObject.GetType());
		TextWriter textWriter = new StringWriter();
		xmlSer.Serialize(textWriter, pObject);
		
		XmlDocument xdoc = new XmlDocument();
		xdoc.LoadXml(textWriter.ToString());
		return xdoc;
	}
	
	// Here we deserialize it back into its original form 
	public object DeserializeObject(string pXmlizedString, XmlSerializer xs)
	{
		MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString));
		//  XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
		return xs.Deserialize(memoryStream);
	}
	
	public static string Encrypt(string toEncrypt)
	{
		byte[] keyArray = UTF8Encoding.UTF8.GetBytes("12345678901234567890123456789012");
		// 256-AES key
		byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
		RijndaelManaged rDel = new RijndaelManaged();
		rDel.Key = keyArray;
		rDel.Mode = CipherMode.ECB;
		// http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
		rDel.Padding = PaddingMode.PKCS7;
		// better lang support
		ICryptoTransform cTransform = rDel.CreateEncryptor();
		byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
		return Convert.ToBase64String(resultArray, 0, resultArray.Length);
	}
	
	public static string Decrypt(string toDecrypt)
	{
		byte[] keyArray = UTF8Encoding.UTF8.GetBytes("12345678901234567890123456789012");
		// AES-256 key
		byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);
		RijndaelManaged rDel = new RijndaelManaged();
		rDel.Key = keyArray;
		rDel.Mode = CipherMode.ECB;
		// http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
		rDel.Padding = PaddingMode.PKCS7;
		// better lang support
		ICryptoTransform cTransform = rDel.CreateDecryptor();
		byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
		return UTF8Encoding.UTF8.GetString(resultArray);
	}
	
}
