﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class PlatformMove : MonoBehaviour {

	public bool grounded;
	bool falling;
	bool hasLost;
	public float grav;
	public float jumpvelocity;
	public float speed;
	public Vector3 velocity;
	public float miny;
	bool isDead = false;
	BoxCollider2D boxCol;
	Rect box;

	string looseText = "You lost! Press e to try again!";

	Vector3 startPosition;

	int layerMask;
	int horizontRays = 6;
	int vertRays = 4;
	float margin = 0.4f;

	public void setDead(){
		if (!isDead) {
			isDead = true;
			falling = true;
			velocity = new Vector3 (0f, 0.1f, 0f);
			grav = 0.1f;
		}
	}

	public void reset(){
		isDead = false;
		falling = false;
		hasLost = false;
		velocity = new Vector3 (0f, 0f, 0f);
		grav = 0.4f;
		gameObject.transform.position = startPosition;
	}

	void youLoose(){
		hasLost = true;
	}

	void OnGUI(){
		Debug.Log ("hasLost: " + hasLost);
		if (hasLost) {
			var width = 250;
			var height = 50;
			var centeredStyle = GUI.skin.GetStyle("Label");
			centeredStyle.fontSize = 20;
			centeredStyle.alignment = TextAnchor.LowerCenter;
			var test = centeredStyle.CalcSize (new GUIContent(looseText));
			var rect = new Rect ((Screen.width)/2 - (test.x/2),(Screen.height) - 60 ,test.x,test.y);
			GUI.Box (rect,"");
			GUI.Label (rect, looseText,centeredStyle);
		}
	}

	// Use this for initialization
	void Start () {
		startPosition = gameObject.transform.position;
		grounded = falling = false;
		boxCol = GetComponent<BoxCollider2D> ();
		velocity = new Vector3 (0, 0, 0);
		jumpvelocity = 9.0f;
		grav = 0.4f;
		speed = 0.1f;
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		var scaledxsize = transform.localScale.x * boxCol.size.x;
		var scaledysize = transform.localScale.y * boxCol.size.y;

		box = new Rect(
			boxCol.transform.position.x- (scaledxsize/2),
		    boxCol.transform.position.y-(scaledysize/2), scaledxsize, scaledysize);

		if(!grounded || isDead)
		{
			velocity.y = velocity.y - grav;
		}

		falling = velocity.y < 0;

		if((grounded || falling) && !isDead)
		{
			Vector2 startPoint = new Vector2(box.xMin + margin, box.center.y);
			Vector2 endPoint = new Vector2(box.xMax - margin, box.center.y);

			float dist = box.height/2 + (grounded ? margin : Mathf.Abs(velocity.y * Time.deltaTime));

			bool contact = false;

			for(int i = 0; i < vertRays; i++)
			{
				Vector2 origin = i==0 ? endPoint : startPoint;

				var hit = Physics2D.Raycast(origin, -Vector2.up, dist);
				if(hit)
				{
					contact = true;
					grounded = true;
					falling = false;
					transform.Translate(-Vector2.up * ((origin.y - hit.point.y) - (box.height/2)));
					velocity.y = 0f;
					break;
				}
			}
			if(!contact){
				grounded = false;
			}
		}
	}
	void LateUpdate()
	{
		if (!isDead) {
			if (grounded && Input.GetKeyDown (KeyCode.Space)) {
				velocity = new Vector3 (velocity.x, jumpvelocity, 0);
				grounded = false;
			}

			var hor = Input.GetAxisRaw ("Horizontal");
			gameObject.transform.Translate (hor * speed * Vector3.right);
		}

		if (hasLost && Input.GetKeyDown (KeyCode.E)) {
			reset ();
		}

		if (gameObject.transform.position.y < miny) {
			setDead ();
			youLoose();
		}

		gameObject.transform.Translate(velocity*Time.deltaTime);
	}
}
