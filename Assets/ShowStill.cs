﻿using UnityEngine;
using System.Collections;

public class ShowStill : MonoBehaviour {

	public string MomLine = "\"Sam, what is this? Look at what you've done to the walls!\"";
	public string[] alternative = new string[]{
	  "I didn't do it! It was Prof. Snuggles!",
	  "I'm sorry. I didn't mean to.",
	  "Why shouldn't I?  I didn't hurt anyone!",
	  "Can I draw somewhere else?"};
	int choice = -1;

	public string[] reply = new string[]{
		"Uh-huh. You can tell the Professor he's grounded if I catch him vandalising my walls again.",
		"You didn't- sweetie, you know what you did wrong, right?",
		"I know you didn't, honey, but the wall isn't for drawing. It just isn't.",
		"I'll find you some paper, alright? I'll be back in a minute, now leave the wall alone."
	};


	int getChoice(){
		return choice;
	}

	void choose(int i){
		choice = i;
	}

	// Use this for initialization
	void Start () {
	
	}

	void OnGUI()
	{
				if (choice == -1) {
						var centeredStyle = GUI.skin.GetStyle ("Label");
						centeredStyle.fontSize = 20;
						centeredStyle.alignment = TextAnchor.UpperLeft;
						GUI.Label (new Rect (120, 10, 400, 100), MomLine, centeredStyle);
						centeredStyle.alignment = TextAnchor.LowerRight;
						GUI.Label (new Rect (320, 140, 400, 200), "Pick a reply: " + "\n" + alternative[0] + ": 1\n" +
								alternative[1] + ": 2\n" + alternative[2] + ": 3\n" + alternative[3] + ": 4\n", centeredStyle);
				} else {
						var centeredStyle = GUI.skin.GetStyle ("Label");
						centeredStyle.fontSize = 20;
						centeredStyle.alignment = TextAnchor.UpperLeft;
						GUI.Label (new Rect (120, 10, 400, 100), reply[choice], centeredStyle);
						GUI.Label (new Rect (220, 100, 400, 100), "Press space to continue", centeredStyle);
				}

	}
	
	// Update is called once per frame
	void Update () {
		StuffContainer stu = GameObject.Find ("TheKeeper").GetComponent<StuffContainer>();
		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)) {
			choose(0);
			stu.getsetevil = true;
			stu.getsetgrownup = false;
			stu.setchoice = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)) {
			choose(1);
			stu.getsetevil = false;
			stu.getsetgrownup = false;
			stu.setchoice = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Keypad3)) {
			choose(2);
			stu.getsetevil = true;
			stu.getsetgrownup = true;
			stu.setchoice = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha4) || Input.GetKeyDown (KeyCode.Keypad4)) {
			choose(3);
			stu.getsetevil = false;
			stu.getsetgrownup = true;
			stu.setchoice = true;
		}
		if (choice != -1 && Input.GetKeyDown (KeyCode.Space)) {
			GameObject.Find ("TheKeeper").GetComponent<LevelManager>().changeLevel(LevelManager.levels.Bane3);

		}
	}
}
